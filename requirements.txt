## ----------- Python version 3.8.5 --------------------
pandas==1.2.4
torch==1.8.1
matplotlib==3.4.3
tqdm==4.59.0
Arabic-Stopwords==0.3
transformers==4.5.1
scikit-learn==0.24.1
openpyxl==3.0.10
numpy==1.19.2
nltk==3.6.3
trectools==0.0.44
urlexpander==0.0.37
tweepy==3.10.0
stanza==1.2.3
tensorboard==2.9.1
snowballstemmer==2.2.0
python-terrier==0.6.0
