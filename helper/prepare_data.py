import Utils


ar_2021_vclaims_dir = "path to vclaims directory"
ar_2022_vclaims_file = 'path to save file path'

# Put all claims in one file for indexing.
Utils.put_all_vclaims_in_one_file(vclaims_dir=ar_2021_vclaims_dir, 
                            vlcaims_file_save_path=ar_2022_vclaims_file)

