All files in this directory are copied from [CheckThat! 2021 repository](https://gitlab.com/checkthat_lab/clef2021-checkthat-lab/-/tree/master/task2). Please cite their paper if you used any of them.

```

@inproceedings{shaar2021overview,
  title={Overview of the CLEF-2021 CheckThat! lab task 2 on detecting previously fact-checked claims in tweets and political debates},
  author={Shaar, Shaden and Haouari, Fatima and Mansour, Watheq and Hasanain, Maram and Babulkov, Nikolay and Alam, Firoj and Da San Martino, Giovanni and Elsayed, Tamer and Nakov, Preslav},
  booktitle={CLEF (Working Notes)},
  year={2021}
}

@inproceedings{nakov2021overview,
  title={Overview of the CLEF--2021 CheckThat! lab on detecting check-worthy claims, previously fact-checked claims, and fake news},
  author={Nakov, Preslav and Da San Martino, Giovanni and Elsayed, Tamer and Barr{\'o}n-Cede{\~n}o, Alberto and M{\'\i}guez, Rub{\'e}n and Shaar, Shaden and Alam, Firoj and Haouari, Fatima and Hasanain, Maram and Mansour, Watheq and others},
  booktitle={International Conference of the Cross-Language Evaluation Forum for European Languages},
  pages={264--291},
  year={2021},
  organization={Springer}
}
```